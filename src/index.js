//Creation des elements
var companyList = undefined;
const search = (e) => {
    e.preventDefault();
    const company = document.getElementById("companyName").value;
    var xhr = new XMLHttpRequest();
    const result = document.getElementById("companyResult");
    
    xhr.onreadystatechange = function(){
        if(xhr.readyState == XMLHttpRequest.DONE){
            const res = JSON.parse(xhr.responseText);
            var tab = "";
            companyList = res.etablissement;
            //lorsqu'on clique sur le bouton, affiche les elements
            res.etablissement.forEach((element, index) => {
                tab += `${element.l1_normalisee}                 
                    <button type="button" onclick="changeCompany(${index})" class="btn btn-outline-info">
                    Informations
                    </button> <br/>
                `
            });

            result.innerHTML = tab;
        }
    };

    //api
    xhr.open("GET", `https://entreprise.data.gouv.fr/api/sirene/v1/full_text/${company}?per_page=5&page=1`, false);
    xhr.send(null);
}

//affichage des informations complementaires
const changeCompany = (index) => {
    let info = companyList[index];
    var render = `
    Nom de l'entreprise : ${info.l1_normalisee}<br/>
    Siren : ${info.siren}<br/>
    Siret : ${info.siret}<br/>
    Adresse :
    ${info.l4_normalisee}, 
    ${info.l6_normalisee}<br/>
    `
    const informationRes = document.getElementById("informationResult");
    informationRes.innerHTML = render;
}